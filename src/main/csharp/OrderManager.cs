﻿using System;
using Microsoft.Extensions.Logging;
using TcpServer.MarketMaker;

namespace TcpServer
{
    public class OrderManager
    {
        public const string BUY = "BUY";
        public const string SELL = "SELL";

        private ILogger<OrderManager> _logger;
        private QuoteCalculationEngine _quoteCalculationEngine;
        private ReferencePriceSource _referencePriceSource;

        public OrderManager(ILogger<OrderManager> logger, QuoteCalculationEngine quoteCalculationEngine, ReferencePriceSource referencePriceSource)
        {
            _logger = logger;
            _quoteCalculationEngine = quoteCalculationEngine;
            _referencePriceSource = referencePriceSource;
        }

        public string GetQuotePriceStr(string incomingMessage)
        {
            string[] parts = incomingMessage.Split(' ');

            // parse incomingMessage
            int securityId = ParseSecurityId(parts[0]);
            bool isBuy = ParseIsBuy(parts[1]);
            int quantity = ParseOrderQuantity(parts[2]);

            double referencePrice = _referencePriceSource.get(securityId);
            double quotePrice = _quoteCalculationEngine.calculateQuotePrice(securityId, referencePrice, isBuy, quantity);
            return quotePrice.ToString(); 
        }

        private static int ParseSecurityId(string securityIdStr)
        {
            if (int.TryParse(securityIdStr, out int securityId))
            {
                return securityId;
            }
            else
            {
                throw new FormatException($"Incorrect format of securityId: {securityIdStr}");
            }
        }

        private static bool ParseIsBuy(string isBuyStr)
        {
            switch (isBuyStr)
            {
                case BUY:
                    return true;
                case SELL:
                    return false;
                default:
                    throw new FormatException($"Incorrect format of isBuyStr: {isBuyStr}");
            }
        }

        private static int ParseOrderQuantity(string orderQuantityStr)
        {
            if (int.TryParse(orderQuantityStr, out int orderQuantity))
            {
                if (orderQuantity <= 0)
                {
                    throw new FormatException($"Invalid quantity (positive integer only): {orderQuantityStr}");
                }
                return orderQuantity;
            }
            else
            {
                throw new FormatException($"Invalid quantity (integer only): {orderQuantityStr}");
            }
        }
    }
}
