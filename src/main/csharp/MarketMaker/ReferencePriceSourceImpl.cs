﻿using System;
using System.Collections.Concurrent;

namespace TcpServer.MarketMaker
{
    public class ReferencePriceSourceImpl : ReferencePriceSource
    {
        private ConcurrentDictionary<int, double> _priceDict;

        public ReferencePriceSourceImpl()
        {
            _priceDict = new ConcurrentDictionary<int, double>();


            // create dummy price
            Random random = new Random();
            int[] securityIds = new int[] { 123, 456, 789 };
            foreach(int securityId in securityIds)
            {
                double price = 100 + 50 * random.NextDouble();
                _priceDict.AddOrUpdate(securityId, price, (k, v) => price);
            }
        }

        public double get(int securityId)
        {
            if (_priceDict.TryGetValue(securityId, out double price))
            {
                return price;
            }
            else
            {
                throw new ArgumentOutOfRangeException(nameof(securityId), $"No reference price for securityId:{securityId}");
            }
        }

        public void subscribe(ReferencePriceSourceListener listener)
        {
            throw new NotImplementedException();
        }
    }
}
