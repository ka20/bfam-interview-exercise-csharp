# Interview Exercise

Implement a server that responds to quote requests.

## Assumptions

1. The number of bytes per clients' request message is less than 1024 bytes;
2. Clients will receive `ERROR: [details]` message if any exceptions 
   (e.g. no quote price is calculated);
3. Negative price should not be used as INVALID as some instruments may have negative price;
4. Only `securityId` as `123`, `456` and `789` are defined; and
5. `quantity` must be a positive integer.

## Description

The server listening to port configured in `appsettings.json` (e.g. `8888`) could be started by running the following command in terminal:

    dotnet BfamTcpServer.dll


The two interfaces (`QuoteCalculationEngine` and `ReferencePriceSource`) with their implementations are located in /marketmarker folder 

* `QuoteCalculationEngine` - its interface is `/MarketMaker/QuoteCalculationEngine.cs` and its implementation is `/MarketMaker/QuoteCalculationEngineImpl.cs` 
* `ReferencePriceSource` - its interface is `/MarketMaker/ReferencePriceSource.cs` and its implementation is `/MarketMaker/ReferencePriceSourceImpl.cs`.


## Testing

Evidence that the server works correctly could be found from `/Log/TcpServer[yyyyMMdd].txt`.

* `123 BUY 23` is valid request
* `123 Buy 23` is INVALID request because the `(BUY|SELL)` must be capital letters
* `123 BUY 23.0` is INVALID request because the `quantity` must be positive integer
* `123 BUY 0` is INVALID request because the `quantity` must be positive integer
* `123 BUY -23` is INVALID request because the `quantity` must be positive integer
* `123 BUY [string]` is INVALID request because the `quantity` must be positive integer
* `[string] BUY [string]` is INVALID request because the `securityId` must be integer
* `234 BUY 23` is INVALID request because the reference price of `securityId`=`234` is not found

