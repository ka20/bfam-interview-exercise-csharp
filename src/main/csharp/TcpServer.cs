﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;

namespace BfamTcpServer
{
    public class TcpServer : IDisposable
    {
        private readonly ILogger<TcpServer> _logger;
        private readonly TcpListener _listener;
        private readonly Func<string, string> _handleMessage;

        public string Host { get; }
        public int Port { get; }

        public TcpServer(ILogger<TcpServer> logger, string host, int port, Func<string, string> handleMessage)
        {
            _logger = logger;
            _handleMessage = handleMessage;

            Host = host ?? throw new ArgumentNullException(nameof(host));
            Port = port;

            _listener = new TcpListener(IPAddress.Parse(Host), Port);
        }

        public void Start()
        {
            _listener.Start();
            _listener.BeginAcceptTcpClient(new AsyncCallback(ClientConnected), null);

        }

        private void ClientConnected(IAsyncResult asyncResult)
        {
            Task.Run(() =>
            {
                using(TcpClient client = _listener.EndAcceptTcpClient(asyncResult))
                {
                    OnHandleTcpClient(client);
                }
            });

            _listener.BeginAcceptTcpClient(new AsyncCallback(ClientConnected), null);
        }

        private void OnHandleTcpClient(TcpClient client)
        {
            string clientIp = client.Client.RemoteEndPoint.ToString();

            try
            {
                if (client.Connected)
                {
                    byte[] incomingBytes = new byte[1024];
                    int length;
                    using (var stream = client.GetStream())
                    {
                        while((length = stream.Read(incomingBytes, 0, incomingBytes.Length))!= 0)
                        {
                            var incomingData = new byte[length];
                            Array.Copy(incomingBytes, 0, incomingData, 0, length);

                            string incomingMessage = Encoding.UTF8.GetString(incomingData);
                            _logger.LogDebug("Incoming message from {IP}: {Message}", clientIp, incomingMessage);

                            string repliedMessage = null;
                            try
                            {
                                repliedMessage = _handleMessage?.Invoke(incomingMessage);
                            }
                            catch(Exception handleMsgException)
                            {
                                repliedMessage = "ERROR: " + handleMsgException.Message;
                            }

                            byte[] repliedBytes = Encoding.UTF8.GetBytes(repliedMessage);
                            if (stream.CanWrite)
                            {
                                stream.Write(repliedBytes, 0, repliedBytes.Length);
                            }
                            else
                            {
                                _logger.LogError("Stream from {IP} is not writable", clientIp);
                            }

                            _logger.LogInformation("{ClientIp} Request={Request} Response={Response}", clientIp, incomingMessage, repliedMessage);
                        }
                    }
                }
            }
            catch (IOException ioException)
            {
                _logger.LogDebug("OnHandleTcpClient {IP} is disconnected: {Message}", clientIp, ioException.Message);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "OnHandleTcpClient {IP} failed with exception", clientIp);
            }
        }


        public void Dispose()
        {
            if (_listener != null)
            {
                _listener.Stop();
            }
        }
    }
}
