﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using TcpServer;
using TcpServer.MarketMaker;

namespace BfamTcpServer
{
    class Program
    {
        static void Main(string[] args)
        {
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);
            var serviceProvider = serviceCollection.BuildServiceProvider();

            var logger = serviceProvider.GetRequiredService<ILogger<Program>>();

            using (TcpServer tcpServer = serviceProvider.GetRequiredService<TcpServer>())
            {
                tcpServer.Start();

                Console.WriteLine("Press ESC key to exit...");
                while (Console.ReadKey().Key != ConsoleKey.Escape)
                {
                }
            }

            logger.LogInformation("TcpServer is closed.");
        }

        static void ConfigureServices(IServiceCollection services)
        {
            // appsettings.json
            var builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory());
            builder.AddJsonFile("appsettings.json", optional: false, reloadOnChange: false);

            // configuration
            var configuration = builder.Build();

            // logger
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .CreateLogger();
            services.AddLogging(config => config.AddSerilog());

            services.AddSingleton<QuoteCalculationEngine>(x => new QuoteCalculationEngineImpl());
            services.AddSingleton<ReferencePriceSource>(x => new ReferencePriceSourceImpl());
            services.AddSingleton<OrderManager>();

            services.AddSingleton<TcpServer>(x =>
                new TcpServer(x.GetRequiredService<ILogger<TcpServer>>(),
                    configuration.GetValue<string>("TcpServer:Host"),
                    configuration.GetValue<int>("TcpServer:Port"),
                    //msg => $"Hi: {msg}"));
                    msg => x.GetRequiredService<OrderManager>().GetQuotePriceStr(msg)));
        }
    }
}
